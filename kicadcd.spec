%if 0%{?fedora}
%global use_python3 1
%global use_python2 0
%global pythonbin %{__python3}
%global python_sitelib %{python3_sitelib}
%else
%global use_python3 0
%global use_python2 1
%if 0%{?__python2:1}
%global pythonbin %{__python2}
%global python_sitelib %{python2_sitelib}
%else
%global pythonbin %{__python}
%global python_sitelib %{python_sitelib}
%endif
%endif
%{!?python_sitelib: %define python_sitelib %(%{pythonbin} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}

Name: kicadcd
Version: 0.0.6
Release: 1%{?dist}
Summary: BOM generator

Group: Development/Tools
License: GPLv3
URL: https://gitlab.cee.redhat.com/ccs-tools/%{name}
Source0: %{name}-%{version}.tar.gz

BuildArch: noarch
%if %{use_python3}
BuildRequires: python3-devel
BuildRequires: python3-setuptools
%else
BuildRequires: python2-devel
BuildRequires: python-setuptools
%endif

%{?python_enable_dependency_generator}

%description
Creates BOM of kicad files

%prep
%setup -q
sed -i 1"s|#!.*|#!%{pythonbin}|" bin/%{name}

%build
%{pythonbin} setup.py build

%install
%{pythonbin} setup.py install -O1 --skip-build --root $RPM_BUILD_ROOT
#rm -f $RPM_BUILD_ROOT%{python_sitelib}/*egg-info/requires.txt


%files
%license LICENSE
%{_bindir}/%{name}
%dir %{python_sitelib}/%{name}
%{python_sitelib}/%{name}/*
%{python_sitelib}/%{name}-*.egg-info


%changelog
* Wed Mar 13 2019 Matej Mužila <mmuzila@redhat.com> 0.0.6-1
- Fix TME issues with component names containing '/' (mmuzila@redhat.com)

* Wed Mar 13 2019 Matej Mužila <mmuzila@redhat.com> 0.0.5-1
- Add encoding meta tag (mmuzila@redhat.com)
- Fix url (mmuzila@redhat.com)

* Tue Mar 12 2019 Matej Mužila <mmuzila@redhat.com> 0.0.4-1
- Fix BS4 dependency (mmuzila@redhat.com)

* Tue Mar 12 2019 Matej Mužila <mmuzila@redhat.com> 0.0.3-1
- 

* Tue Mar 12 2019 Matej Mužila <mmuzila@redhat.com> 0.0.2-1
- new package built with tito

* Tue Mar 12 2019 Matej Mužila <mmuzila@gmail.com> - 0.0.1-1
- Intial build
