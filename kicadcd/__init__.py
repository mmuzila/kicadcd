import sys
import os
import argparse
from .sch import Schematic
from .sch import Component
from pprint import pprint
from tabulate import tabulate
from enum import Enum
import re
from urllib.request import urlopen
from urllib.request import Request

from bs4 import BeautifulSoup as BS

HEADERS = {
        'User-Agent':'Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:63.0) Gecko/20100101 Firefox/63.0',
        }

class ShopType(Enum):
    OTHER = 0
    TME = 1
    GME = 2

class Shop():
    shop_type = ShopType.OTHER
    def __init__(self, ident):
        self.ident = ident

    def __str__(self):
        if self.shop_type == ShopType.OTHER:
            return ""
        return "{}:{}".format(self.shop_type.name, self.ident)

    def html(self):
        return '<a>{}</a>'.format(self.ident)

class ShopGME(Shop):
    shop_type = ShopType.GME
    base_addr = "https://www.gme.cz/"

    def html(self):
        req = Request(self.base_addr + 'inc/ajax/asearch?search_keyword={}'\
                .format(self.ident), headers = HEADERS)
        resp = urlopen(req).read()
        soup = BS(resp, 'html.parser')
        item = soup.find('div', {'class':'product-item'})

        name = soup.find('span', {'class' : 'title'}).text
        link = item.find('a')['href']

        return '<a href="{}/{}">{}</a>'.format(self.base_addr, link, name)


class ShopTME(Shop):
    shop_type = ShopType.TME

    def html(self):

        ident = self.ident.replace('/', '_')

        return '<a href="https://www.tme.eu/cz/details/{}">{}</a>'.format(
                ident, self.ident)

def getShop(instr):

    idents = {
                "TME" : ShopTME,
                "GME" : ShopGME,
            }

    mtch = re.match(r"([^:]+):(.*)", instr)

    if mtch is None:
        return Shop(instr)

    typestr = mtch.group(1)
    ident = mtch.group(2)

    try:
        return idents[typestr](ident)
    except:
        raise ValueError("Shop type not known: {}".format(typestr))


class MyComponent(Component):
    def __init__(self, comp):
        self.labels = comp.labels
        self.unit = comp.unit
        self.position = comp.position
        self.references = comp.references
        self.fields = comp.fields
        self.old_stuff = comp.old_stuff

    @property
    def value(self):
        return self.field_by_id('1')

    @property
    def reference(self):
        return self.field_by_id('0')

    @property
    def footprint(self):
        return self.field_by_id('2')

    @property
    def datasheet(self):
        return self.field_by_id('3')

    @property
    def datasheet(self):
        return self.field_by_id('3')

    @property
    def shop(self):
        try:
            shop = getShop(self.field_by_name('Shop'))
        except KeyError:
            return Shop("")
        return shop

    def field_by_id(self, strid):
        for i in self.fields:
            if i['id'] == strid:
                return i['ref'][1:-1]
        raise KeyError("Unable to find field by id: {}".format(strid))

    def field_by_name(self, strname):
        for i in self.fields:
            if i['name'][1:-1] == strname:
                return i['ref'][1:-1]
        raise KeyError("Unable to find field by name: {}".format(strname))



def init_argparser():
    parser = argparse.ArgumentParser()
    parser.add_argument("-H", "--html",
                        help="Output html",
                        action="store_true")

    parser.add_argument('input', nargs=1)

    return parser

def get_components(schema, dirname):
    components = schema.components

    for sheet in schema.sheets:
        subsch_name = sheet.fields[1]['value'][1:-1]
        subsch_name = os.path.join(dirname, subsch_name)
        subsch = Schematic(subsch_name)
        components.extend(get_components(subsch, dirname))

    return components

def to_html(obj):
    if hasattr(obj, "html") and callable(getattr(obj, "html")):
        return obj.html()
    else:
        return str(obj)


def app():
    parser = init_argparser()
    args = parser.parse_args()

    schema = Schematic(args.input[0])
    dirname = os.path.dirname(args.input[0])

    components = get_components(schema, dirname)


    comp_table = list()
    for c in components:
        c = MyComponent(c)
        if c.reference.startswith('#'):
            continue
        line = [c.reference, c.value, c.shop, c.footprint, c.datasheet]

        if args.html:
            line = list(map(to_html, line))
        else:
            line = list(map(str, line))

        comp_table.append(line)

    prefix = ""
    suffix = ""

    if args.html:
        prefix = '<html><head><meta charset="utf-8"></head><body>'
        suffix = '</body></html>'
        tablefmt = 'html'
    else:
        tablefmt = 'plain'

    strtable = prefix + tabulate(comp_table, tablefmt=tablefmt) + suffix

    print(strtable)




