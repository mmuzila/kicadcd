# -*- coding: utf-8 -*-

from setuptools import setup, find_packages
#from distutils.core import setup

with open('LICENSE', encoding='utf-8') as f:
    license = f.read()

description='BOM generator for kicad'

setup(
    name='kicadcd',
    version='0.0.6',
    description=description,
    long_description=description,
    author='Matej Mužila',
    author_email='mmuzila@gmail.com',
    scripts=['bin/kicadcd'],
    url='https://gitlab.com/mmuzila/kicadcd',
    license=license,
    install_requires=[
        'tabulate',
        'beautifulsoup4',
    ],
    packages=['kicadcd'],
    include_package_data=True,
    package_dir={'kicadcd': 'kicadcd'},
)
